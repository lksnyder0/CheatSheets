# Tmux bindings
prefix: ctrl + a

| Function | Keys |
| -------- | -------- |
| New Window | prefix + c |
| Horizontal Split | bind + b |
| Vertical Split | bind + v |
| Move pane | bind <arrow key> |


# [tmux-record](https://github.com/tmux-plugins/tmux-logging)
| Function | Keys |
| -------- | -------- |
| Logging | prefix + shift + p |
| Screen Capture | prefix + alt + p |
| Save Complete History | prefix + alt + shift + p |
| Clear Pane History | prefix + alt + c |
