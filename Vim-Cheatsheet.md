# Editing

| Function | Keys |
| -------- | -------- |
| Insert   | i   |
| Insert at beginning of line   | I   |
| Insert after curser | a |
| Insert at end of line | A |
| Delete character | d |
| Delete to end of word | dw |
| Delete to end of row | D or d$ |
| Delete line | dd |
| Delete between characters | di{character} |
| Delete until (including) character | df{character} |

# Panes
| Function | Keys |
| -------- | -------- |
| Move to pane | ctrl-w arrow |
| Pervious Pane | ctrl-w ctrl-w |
| resize pane | ctrl-w  {n}[-,+] |
| horizontal split | :sp <file> |
| vertical split | :vsp <file> |
| increase pane size | <n> ctrl-w + |
| increase size of vertial pane | <n> ctrl-w > |
| decrease size of vertical pane | <n> ctrl-w < |
| move pane up/left | ctrl-w R |
| move pane down/right | ctrl-w r |
| move pane to very bottom | ctrl-w J |
| move pane to very top | ctrl-w K |
| move pane to far right | ctrl-w L |
| move pane to far left | ctrl-w H |

# Tabs
| Function | Keys |
| -------- | -------- |
| Next Tab | gt |
| Previous Tab | gT |
| Move to tab | {i} gt |

# Plugins
## [NERDTree](https://github.com/jistr/vim-nerdtree-tabs)
| Function | Keys |
| -------- | -------- |
| Open file in new tab | t |
| Open file in new tab (background) | T |
| Modify element | m |

## [Fugitive](https://github.com/tpope/vim-fugitive)
| Function | Keys |
| -------- | -------- |
| Stage change | :Gwrite |
| Revert changes | :Gread |
| Remove a file | :Gremove |
| Move a file | :Gmove |
| Arbitrary git command | :Git <command> |
| More Help | :help :Git |
